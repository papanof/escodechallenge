﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsCodeChallenge.ViewModels
{
    public class Version : IComparable
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Release { get; set; }

        public Version(string versionStr)
        {
            if (!string.IsNullOrWhiteSpace(versionStr))
            {
                var split = versionStr.Split('.');

                if (split.Length > 0)
                {
                    int.TryParse(split[0], out int tempMajor);
                    this.Major = tempMajor;
                }

                if (split.Length > 1)
                {
                    int.TryParse(split[1], out int tempMinor);
                    this.Minor = tempMinor;
                }

                if (split.Length > 2)
                {
                    int.TryParse(split[2], out int tempRelease);
                    this.Release = tempRelease;
                }
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Version otherVersion = obj as Version;

            if (otherVersion != null)
            {
                if (this.Major != otherVersion.Major)
                {
                    return this.Major.CompareTo(otherVersion.Major);
                }

                if (this.Minor != otherVersion.Minor)
                {
                    return this.Minor.CompareTo(otherVersion.Minor);
                }

                if (this.Release != otherVersion.Release)
                {
                    return this.Release.CompareTo(otherVersion.Release);
                }

                return 0;
            }
            else
            {
                throw new ArgumentException("Object is not a Version");
            }
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}.{2}", this.Major.ToString(), this.Minor.ToString(), this.Release.ToString());
        }

        public static bool operator >(Version operand1, Version operand2)
        {
            return operand1.CompareTo(operand2) == 1;
        }

        // Define the is less than operator.
        public static bool operator <(Version operand1, Version operand2)
        {
            return operand1.CompareTo(operand2) == -1;
        }

        // Define the is greater than or equal to operator.
        public static bool operator >=(Version operand1, Version operand2)
        {
            return operand1.CompareTo(operand2) >= 0;
        }

        // Define the is less than or equal to operator.
        public static bool operator <=(Version operand1, Version operand2)
        {
            return operand1.CompareTo(operand2) <= 0;
        }
    }


}
