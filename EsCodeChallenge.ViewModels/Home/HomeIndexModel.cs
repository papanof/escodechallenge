﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EsCodeChallenge.ViewModels.Home
{
    public class HomeIndexModel
    {
        public ICollection<Software> Software { get; set; }

        public SearchModel Search { get; set; }

        public HomeIndexModel()
        {
            this.Software = new List<Software>();
            this.Search = new SearchModel();
        }

        public HomeIndexModel(IEnumerable<Software> softwareList)
        {
            this.Search = new SearchModel();

            if (softwareList != null) {
                this.Software = softwareList.ToList();
            }
        }
    }
}
