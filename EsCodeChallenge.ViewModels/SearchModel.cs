﻿using EsCodeChallenge.ViewModels.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsCodeChallenge.ViewModels
{
    public class SearchModel
    {
        [Required]
        [VersionNumber]
        public string VersionSearchStr { get; set; }
    }
}
