﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EsCodeChallenge.ViewModels.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    sealed public class VersionNumber : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool result = false;

            try
            {
                string versionNumber = value.ToString();

                Regex regex = new Regex(@"^\d*\.?\d*\.?\d*$");
                Match match = regex.Match(versionNumber);

                result = match.Success;
            }
            catch
            {
                // gulp, yum!
            }
  
            return result;
        }
    }
}
