﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsCodeChallenge.ViewModels
{
    public class Software
    {
        public string Name { get; set; }
        public Version Version { get; set; }

        public Software(Data.Models.Software software)
        {
            this.Name = software.Name;
            this.Version = new Version(software.Version);
        }
    }
}
