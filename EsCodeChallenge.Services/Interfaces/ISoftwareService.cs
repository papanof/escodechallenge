﻿using EsCodeChallenge.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsCodeChallenge.Services.Interfaces
{
    public interface ISoftwareService
    {
        ICollection<Software> findSoftwareGreaterThan(string searchStr);
        ICollection<Software> getAll();
    }
}
