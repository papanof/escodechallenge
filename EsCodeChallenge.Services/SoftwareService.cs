﻿using EsCodeChallenge.Data;
using EsCodeChallenge.Services.Interfaces;
using EsCodeChallenge.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsCodeChallenge.Services
{

    public class SoftwareService : ISoftwareService
    {
        private string cleanSearchString(string searchStr)
        {
            if (!string.IsNullOrEmpty(searchStr))
            {
                searchStr = searchStr.Trim();
                searchStr = searchStr.Trim('.');
            }

            return searchStr;
        }

        public ICollection<Software> findSoftwareGreaterThan(string searchStr)
        {
            searchStr = cleanSearchString(searchStr);
 
            if (string.IsNullOrWhiteSpace(searchStr))
            {
                return new List<Software>();
            }

            ViewModels.Version searchVersion = new ViewModels.Version(searchStr);

            var list = SoftwareManager.GetAllSoftware().ToList();

            var softwareItems = new List<Software>();
            list.ForEach(x => softwareItems.Add(new Software(x)));

            var filteredList = softwareItems.Where(x => x.Version > searchVersion);

            return filteredList.ToList();

        }

        public ICollection<Software> getAll()
        {
            var list = SoftwareManager.GetAllSoftware().ToList();
            var softwareItems = new List<Software>();
            list.ForEach(x => softwareItems.Add(new Software(x)));

            return softwareItems.ToList();
        }
    }
}
