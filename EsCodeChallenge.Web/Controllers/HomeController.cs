﻿using EsCodeChallenge.Services;
using EsCodeChallenge.Services.Interfaces;
using EsCodeChallenge.ViewModels;
using EsCodeChallenge.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EsCodeChallenge.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // TODO: Inject service into controller with DI
            ISoftwareService softwareService = new SoftwareService();
        
            var softwareList = softwareService.getAll();

            HomeIndexModel viewModel = new HomeIndexModel(softwareList);

            return View(viewModel);
        }

        public ActionResult Search(SearchModel search)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            // TODO: Inject service into controller with DI
            ISoftwareService softwareService = new SoftwareService();

            var softwareList = softwareService.findSoftwareGreaterThan(search.VersionSearchStr);

            HomeIndexModel viewModel = new HomeIndexModel(softwareList);

            return View("Index", viewModel);
        }
    }
}