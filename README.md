# Emergent Software Code Challenge

## Approach

I decided that running this challenge like an agile project would be an interesting way to tackle the problem and hopefully will 
result in better code. I'm going to break up my time into four, one hour sprints. 
I'll be using an egg timer to track each sprint and be taking break after each sprint.

Here's what I hope to accomplish in each sprint, I'll adjust if I need to after each sprint. The way I'm laying this out is I should be working 
on the most critical items first.

### Sprint 1 (Project Setup):

* Create empty ASP.NET MVC project & check into git
* Create project architecture (create & add projects to solution, solidify references) 
* Copy in SoftwareManager code from Emergent

### Sprint 2 (Core algorithm): 

* Build core service for search algorithm   

### Sprint 3 (Create bare-bones pages & wire them up):

* Create page that submits form w/search string
* Create control that lists Software
* Wire up search service to form submit & listing control

### Sprint 4 (Polish/Stretch Goals):

* Add custom validator for search text box
* Add some CSS/core styles so it doesn't look awful
* Unit test core algorithm
* Add API that exposes core service

## Running

* Built in Visual Studio Community 2017
* Clone
* F5
* http://localhost:50100/


## Sprint Log

### Sprint 1 Notes
Accomplished all my goals but I'm not happy on how the ViewModels turned out; I'll try to revisit them in sprint 3 when I actually create the page(s)

### Sprint 2 Notes
Completed all sprint goals and am pretty happy about the heavy lifting being handled by a IComparable version class. Also had some extra time to wire up the Core service with hard-coded values.

### Sprint 3 Notes
Got everything done I wanted to and also got to work ahead on some sprint 4 tasks. Site works, just need to add some basic styling (maybe just use the built-in bootstrap components) and try to improve on code quality   

### Sprint 4 Notes
Did some basic styling & some minor code cleanup, adding completing readme and getting ready to ship. NOT BAD.
